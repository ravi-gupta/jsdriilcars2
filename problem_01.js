function getCarWithById(inventory, id) {
  if (!Array.isArray(inventory) || typeof id !== "number") {
    console.log("Input data is not valid... Please feed valid data");
    return;
  }
  let output = inventory.filter((car) => {
    return car.id === id;
  });

  carById = output[0];

  if (carById) {
    console.log(
      `Car 33 is a ${carById.car_year} ${carById.car_make} ${carById.car_model}`
    );
  } else {
    console.log("Car with this id not found.");
  }
  return;
}
module.exports = getCarWithById;
