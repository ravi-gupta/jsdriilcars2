function getCarYears(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input data is not valid... Please feed valid data";
  }
  if (inventory.length === 0) {
    return "Input data is empty";
  }
  let carYears = inventory.map((car) => {
    return car.car_year;
  });
  return carYears;
}
module.exports = getCarYears;
