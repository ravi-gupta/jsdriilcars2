function getOldCars(inventory, year) {
  if (!Array.isArray(inventory) || typeof year !== "number") {
    return "Input data is not valid... Please feed valid data";
  }
  if (inventory.length === 0) {
    return "Input data is empty";
  }

  const oldCars = inventory.filter((car) => {
    return car.car_year < year;
  });
  console.log(`Total Cars which is older than 2000 : ${oldCars.length}`);
  return oldCars;
}
module.exports = getOldCars;
