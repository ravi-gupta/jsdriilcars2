function getLastCar(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Input data is not valid");
    return;
  }
  let lastCar = inventory[inventory.length - 1];
  if (lastCar) {
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  } else {
    console.log("No cars in inventory.");
  }
  return;
}
module.exports = getLastCar;
