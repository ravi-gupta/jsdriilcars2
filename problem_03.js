function getCarModels(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input data is not valid... Please feed valid data";
  }
  if (inventory.length === 0) {
    return "Input data is empty";
  }
  const models = inventory
    .map((car) => {
      return car.car_model;
    })
    .sort((a, b) => {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });
  return models;
}
module.exports = getCarModels;
