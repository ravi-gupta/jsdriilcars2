function getBmwAudi(inventory, car1, car2) {
  if (
    !Array.isArray(inventory) ||
    typeof car1 !== "string" ||
    typeof car2 !== "string"
  ) {
    return "Input data is not valid... Please feed valid data";
  }
  if (inventory.length === 0) {
    return "Input data is empty";
  }

  const BMWAndAudi = inventory.filter((car) => {
    return (
      car.car_make.toLowerCase() === car1.toLowerCase() ||
      car.car_make.toLowerCase() === car2.toLowerCase()
    );
  });
  return BMWAndAudi;
}
module.exports = getBmwAudi;
